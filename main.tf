provider "aws" {
region = "ap-south-1"
}
resource "aws_instance" "example" {
  ami           = "ami-0bcf5425cdc1d8a85"
  instance_type = "t2.micro"
  key_name      = "wezva"
  vpc_security_group_ids=["sg-06cf4f8ada7aec2dd"]
provisioner "local-exec" {
  command="sleep 30; ansible-playbook build.yml -i ${aws_instance.example.private_ip}, -u ec2-user --key-file /home/ec2-user/terrajenk/wezva.pem"
}
provisioner "local-exec" {
  command="sleep 30; ansible-playbook admin.yml -i ${aws_instance.example.private_ip}, -u ec2-user --key-file /home/ec2-user/terrajenk/wezva.pem"
}
}
output "ec2-dns" {
  value=aws_instance.example.public_dns
}

